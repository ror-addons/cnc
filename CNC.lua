CNC = {}

--	REGISTER EVENTS
function CNC.OnInit()
	RegisterEventHandler(SystemData.Events.PLAYER_COMBAT_FLAG_UPDATED, "CNC.CombatCheck")
end

function CNC.CombatCheck()
	if (GameData.Player.inCombat) then
		SystemData.Settings.Names.enemynpcs = false 	-- HIDE ENEMY NAMES
	else
		SystemData.Settings.Names.enemynpcs = true		-- SHOW ENEMY NAMES
	end
	BroadcastEvent( SystemData.Events.USER_SETTINGS_CHANGED )	-- APPLY CHANGES
end
